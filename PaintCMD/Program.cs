﻿using System;
using System.Collections.Generic;

namespace PaintCMD
{
    class Program
    {
        public enum ColorList { Biały, Niebieski, Zielony, Czerwony, Żółty }
        public static ColorList colorList = ColorList.Biały;

        public enum TriangleType { RectangularToRight, RectangularToLeft, Equilateral };

        static List<Rectangle> rects = new List<Rectangle>();
        static List<Triangle> triangles = new List<Triangle>();
        static bool isFinished = false;

        static ConsoleColor GetColorFromList(ColorList cList)
        {
            switch (cList)
            {
                case ColorList.Biały:
                    return ConsoleColor.White;
                case ColorList.Niebieski:
                    return ConsoleColor.Blue;
                case ColorList.Zielony:
                    return ConsoleColor.Green;
                case ColorList.Czerwony:
                    return ConsoleColor.Red;
                case ColorList.Żółty:
                    return ConsoleColor.Yellow;
                default:
                    return ConsoleColor.White;
            }
        }

        struct Rectangle
        {
            public int width, height, xPos, yPos;
            public ColorList color;


            public void AddRect()
            {
                Console.Clear();

                Console.Write("Podaj szerokość: ");
                width = int.Parse(Console.ReadLine());

                Console.Write("Podaj wysokość: ");
                height = int.Parse(Console.ReadLine());

                Console.Write("Podaj współrzędną x: ");
                xPos = int.Parse(Console.ReadLine())*2;

                Console.Write("Podaj współrzędną y: ");
                yPos = int.Parse(Console.ReadLine());

                Console.WriteLine("Dostępne kolory:");
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine(i + 1 + "] " + Enum.GetName(typeof(ColorList), i));
                }
                Console.Write("Podaj kolor: ");
                switch (int.Parse(Console.ReadLine()))
                {
                    case 1:
                        color = ColorList.Biały;
                        break;
                    case 2:
                        color = ColorList.Niebieski;
                        break;
                    case 3:
                        color = ColorList.Zielony;
                        break;
                    case 4:
                        color = ColorList.Czerwony;
                        break;
                    case 5:
                        color = ColorList.Żółty;
                        break;
                }

                rects.Add(this);
                Console.Clear();
            }

            public void AlternativeAddRect()
            {
                Console.Clear();
                xPos = 0;
                yPos = 0;
                
                

                Console.Write("Podaj szerokość: ");
                while(!int.TryParse(Console.ReadLine(), out width) || width <= 0)
                {
                    Console.Clear();
                    Console.Write("Podaj szerokość: ");
                }

                Console.Write("Podaj wysokość: ");
                while (!int.TryParse(Console.ReadLine(), out height) || height <= 0)
                {
                    Console.Clear();
                    Console.Write("Podaj wysokość: ");
                }

                Console.WriteLine("Dostępne kolory:");
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine(i + 1 + "] " + Enum.GetName(typeof(ColorList), i));
                }
                Console.Write("Podaj kolor: ");
                int colorIndex;
                while (!int.TryParse(Console.ReadLine(), out colorIndex) || colorIndex <= 0 || colorIndex > 5)
                {
                    Console.Clear();
                    Console.WriteLine("Dostępne kolory:");
                    for (int i = 0; i < 5; i++)
                    {
                        Console.WriteLine(i + 1 + "] " + Enum.GetName(typeof(ColorList), i));
                    }

                    Console.Write("Podaj kolor: ");
                }

                switch (colorIndex)
                {
                    case 1:
                        color = ColorList.Biały;
                        break;
                    case 2:
                        color = ColorList.Niebieski;
                        break;
                    case 3:
                        color = ColorList.Zielony;
                        break;
                    case 4:
                        color = ColorList.Czerwony;
                        break;
                    case 5:
                        color = ColorList.Żółty;
                        break;
                }

                ConsoleKey keyboardInput = ConsoleKey.NoName;

                while (keyboardInput != ConsoleKey.Enter && keyboardInput != ConsoleKey.Escape)
                {
                    Console.Clear();
                    DrawPicture(false);
                    Console.SetCursorPosition(xPos,yPos);
                    DrawRect();
                    keyboardInput = Console.ReadKey(true).Key;
                    switch (keyboardInput)
                    {
                        case ConsoleKey.W:
                            if (Console.CursorTop - height > 0)
                            {
                                yPos--;
                            }
                            break;
                        case ConsoleKey.UpArrow:
                            if (Console.CursorTop - height > 0)
                            {
                                yPos--;
                            }
                            break;

                        case ConsoleKey.A:
                            if (Console.CursorLeft > 0)
                            {
                                xPos--;
                            }
                            break;
                        case ConsoleKey.LeftArrow:
                            if (Console.CursorLeft > 0)
                            {
                                xPos--;
                            }
                            break;

                        case ConsoleKey.S:
                            if (Console.CursorTop < Console.WindowHeight-1)
                            {
                                yPos++;
                            }
                            break;
                        case ConsoleKey.DownArrow:
                            if (Console.CursorTop < Console.WindowHeight - 1)
                            {
                                yPos++;
                            }
                            break;

                        case ConsoleKey.D:
                            if (Console.CursorLeft + (width*2 - 1) < Console.WindowWidth)
                            {
                                xPos++;
                            }
                            break;
                        case ConsoleKey.RightArrow:
                            if (Console.CursorLeft + (width * 2 - 1) < Console.WindowWidth)
                            {
                                xPos++;
                            }
                            break;
                        default:
                            break;
                    }
                }
                if (keyboardInput == ConsoleKey.Enter)
                {
                    rects.Add(this);
                }

            }

            public void DrawRect()
            {
                Console.ForegroundColor = GetColorFromList(color);
                for (int j = 0; j < height; j++)
                {
                    for (int k = 0; k < width; k++)
                    {
                        if (j == 0 || j == height - 1)
                        {
                            //Console.SetCursorPosition(xPos, yPos);
                            if (k == width - 1)
                            {
                                Console.Write("*");
                            }
                            else
                            {
                                Console.Write("*");
                                Console.SetCursorPosition(Console.CursorLeft + 1, Console.CursorTop);
                            }

                        }
                        else if (k == 0)
                        {
                            Console.SetCursorPosition(xPos, yPos + j);
                            Console.Write("*");
                        }
                        else if (k == width - 1)
                        {
                            Console.SetCursorPosition(xPos + k * 2, yPos + j);
                            Console.Write("*");
                        }
                    }
                    Console.SetCursorPosition(xPos, yPos + j + 1);
                }
                Console.ForegroundColor = ConsoleColor.White;
            }

            public void RemoveFromList()
            {
                Console.Clear();
                if (rects.Count > 0)
                {
                    Console.WriteLine("Podaj indeks prostokąta [1:{0}].", rects.Count);
                    int removeIndex = int.Parse(Console.ReadLine());
                    if (removeIndex > 0 && removeIndex <= rects.Count)
                    {
                        rects.RemoveAt(removeIndex-1);
                    }
                    else
                    {
                        Console.WriteLine("Błędny indeks!");
                        Console.ReadKey(true);
                    }

                } else
                {
                    Console.Write("Brak prostokątów do usunięcia!");
                    Console.ReadKey(true);
                }

                Console.Clear();
            }
        }

        struct Triangle
        {
            public int edgeLength, xPos, yPos;
            public ColorList color;
            public TriangleType triangleType;

            public void AddTriangle()
            {
                Console.Clear();
                Console.Write("Podaj długość boku trójkąta: ");
                edgeLength = int.Parse(Console.ReadLine());

                Console.Write("Podaj współrzędną x: ");
                xPos = int.Parse(Console.ReadLine())*2;

                Console.Write("Podaj współrzędną y: ");
                yPos = int.Parse(Console.ReadLine());

                Console.WriteLine("Dostępne kolory:");
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine(i + 1 + "] " + Enum.GetName(typeof(ColorList), i));
                }
                Console.Write("Podaj kolor: ");
                switch (int.Parse(Console.ReadLine()))
                {
                    case 1:
                        color = ColorList.Biały;
                        break;
                    case 2:
                        color = ColorList.Niebieski;
                        break;
                    case 3:
                        color = ColorList.Zielony;
                        break;
                    case 4:
                        color = ColorList.Czerwony;
                        break;
                    case 5:
                        color = ColorList.Żółty;
                        break;
                }

                triangles.Add(this);
                Console.Clear();
            }

            public void AlternativeAddTriangle()
            {
                Console.Clear();
                xPos = 0;
                yPos = 0;

                Console.WriteLine("Typy trójkątów:");

                Triangle t = new Triangle();
                t.xPos = 1;
                t.yPos = 2;
                t.edgeLength = 4;

                t.triangleType = TriangleType.RectangularToRight;
                t.DrawTriangle();
                Console.SetCursorPosition(3, 6);
                Console.Write("[1]");

                t.xPos = 10;
                t.yPos = 2;
                t.triangleType = TriangleType.Equilateral;
                t.DrawTriangle();
                Console.SetCursorPosition(12, 6);
                Console.Write("[2]");

                Console.SetCursorPosition(0, 8);
                Console.Write("Podaj typ: ");
                int triangleTypeChoice;
                while (!int.TryParse(Console.ReadLine(), out triangleTypeChoice) || triangleTypeChoice <= 0 || triangleTypeChoice > 2)
                {
                    Console.SetCursorPosition(0, 8);
                    Console.Write("                        ");
                    Console.SetCursorPosition(0, 8);
                    Console.WriteLine("Podaj typ: ");
                    Console.SetCursorPosition(11, 8);
                }

                switch (triangleTypeChoice)
                {
                    case 1:
                        triangleType = TriangleType.RectangularToRight;
                        break;
                    case 2:
                        triangleType = TriangleType.Equilateral;
                        break;
                    default:
                        triangleType = TriangleType.RectangularToRight;
                        break;
                }

                Console.SetCursorPosition(0, 9);
                Console.Write("Podaj długość boku: ");
                edgeLength = int.Parse(Console.ReadLine());

                Console.WriteLine("Dostępne kolory:");
                for (int i = 0; i < 5; i++)
                {
                    Console.WriteLine(i + 1 + "] " + Enum.GetName(typeof(ColorList), i));
                }
                Console.Write("Podaj kolor: ");
                switch (int.Parse(Console.ReadLine()))
                {
                    case 1:
                        color = ColorList.Biały;
                        break;
                    case 2:
                        color = ColorList.Niebieski;
                        break;
                    case 3:
                        color = ColorList.Zielony;
                        break;
                    case 4:
                        color = ColorList.Czerwony;
                        break;
                    case 5:
                        color = ColorList.Żółty;
                        break;
                }

                ConsoleKey keyboardInput = ConsoleKey.NoName;

                while (keyboardInput != ConsoleKey.Enter && keyboardInput != ConsoleKey.Escape)
                {
                    Console.Clear();
                    DrawPicture(false);
                    Console.SetCursorPosition(xPos, yPos);
                    DrawTriangle();
                    keyboardInput = Console.ReadKey(true).Key;
                    switch (keyboardInput)
                    {
                        case ConsoleKey.W:
                            if (Console.CursorTop - edgeLength > 0)
                            {
                                yPos--;
                            }
                            break;
                        case ConsoleKey.UpArrow:
                            if (Console.CursorTop - edgeLength > 0)
                            {
                                yPos--;
                            }
                            break;

                        case ConsoleKey.A:
                            if (Console.CursorLeft > 0)
                            {
                                xPos--;
                            }
                            break;
                        case ConsoleKey.LeftArrow:
                            if (Console.CursorLeft > 0)
                            {
                                xPos--;
                            }
                            break;

                        case ConsoleKey.S:
                            if (Console.CursorTop < Console.WindowHeight - 1)
                            {
                                yPos++;
                            }
                            break;
                        case ConsoleKey.DownArrow:
                            if (Console.CursorTop < Console.WindowHeight - 1)
                            {
                                yPos++;
                            }
                            break;

                        case ConsoleKey.D:
                            if (Console.CursorLeft + (edgeLength * 2 - 1) < Console.WindowWidth)
                            {
                                xPos++;
                            }
                            break;
                        case ConsoleKey.RightArrow:
                            if (Console.CursorLeft + (edgeLength * 2 - 1) < Console.WindowWidth)
                            {
                                xPos++;
                            }
                            break;
                        default:
                            break;
                    }
                }
                if (keyboardInput == ConsoleKey.Enter)
                {
                    triangles.Add(this);
                }

            }

            public void DrawTriangle()
            {
                Console.ForegroundColor = GetColorFromList(color);

                /* Draws triangle of type 
                 *  *  
                 *  * *
                 *  *   *
                 *  * * * *
                 */
                if (triangleType == TriangleType.RectangularToRight) {
                    for (int currentHeight = 0; currentHeight < edgeLength; currentHeight++)
                    {
                        for (int currentWidth = 0; currentWidth <= currentHeight; currentWidth++)
                        {
                            if (currentHeight == edgeLength - 1) // If lowest(last) row of triangle, draw line of "*"
                            {
                                Console.Write("*");
                                Console.SetCursorPosition(Console.CursorLeft + 1, Console.CursorTop);
                            }
                            else if (currentWidth == 0) // If first element in row, draw single "*"
                            {
                                Console.SetCursorPosition(xPos, yPos + currentHeight);
                                Console.Write("*");
                            }
                            else if (currentWidth == currentHeight) // If last element in row, draw single "*"
                            {
                                Console.SetCursorPosition(xPos + currentWidth * 2, yPos + currentHeight);
                                Console.Write("*");
                            }
                        }
                        Console.SetCursorPosition(xPos, yPos + currentHeight + 1);
                    }
                }
                /* Draws triangle of type 
                 *        *
                 *      * *
                 *    *   *
                 *  * * * *
                 */

                /* Draws triangle of type 
                 *     *  
                 *    * *
                 *   * * *
                 *  * * * *
                 */
                if (triangleType == TriangleType.Equilateral)
                {
                    int leftOffset = 0;
                    for (int currentHeight = edgeLength-1; currentHeight >= 0; currentHeight--)
                    {
                        Console.SetCursorPosition(xPos + leftOffset, yPos + currentHeight);
                        for (int currentWidth = 0; currentWidth <= currentHeight; currentWidth++)
                        {
                            if (currentHeight == edgeLength-1)
                            {
                                Console.Write("*");
                            }
                            else if (currentWidth == 0 || currentWidth == currentHeight)
                            {
                                Console.SetCursorPosition(xPos + leftOffset + currentWidth * 2, Console.CursorTop);
                                Console.Write("*");
                            }

                            Console.SetCursorPosition(Console.CursorLeft + 1, Console.CursorTop);
                        }
                        leftOffset++;
                        Console.SetCursorPosition(xPos, yPos + currentHeight + 1);
                    }
                }

                Console.ForegroundColor = ConsoleColor.Gray;
            }

            public void RemoveFromList()
            {
                Console.Clear();
                Console.WriteLine("Podaj indeks trójkąta [1:{0}]", triangles.Count);
                triangles.RemoveAt(int.Parse(Console.ReadLine()) - 1);
                Console.Clear();
            }
        }

        static void DrawPicture(bool drawWithDelay)
        {
            Console.CursorVisible = false;
            Console.Clear();

            //Draw all rectangles from list
            for (int i = 0; i < rects.Count; i++)
            {
                Console.SetCursorPosition(rects[i].xPos, rects[i].yPos);
                Console.ForegroundColor = GetColorFromList(rects[i].color);

                rects[i].DrawRect();
                if(drawWithDelay)System.Threading.Thread.Sleep(200);
            }

            //Draw all triangles from list
            for (int i = 0; i < triangles.Count; i++)
            {
                Console.SetCursorPosition(triangles[i].xPos, triangles[i].yPos);
                Console.ForegroundColor = GetColorFromList(triangles[i].color);

                triangles[i].DrawTriangle();
                if(drawWithDelay)System.Threading.Thread.Sleep(200);
            }

            Console.SetCursorPosition(0, 0);
            //Console.ReadKey(true);
        }


        static void Main(string[] args)
        {
            Console.CursorVisible = true;
            Console.SetWindowSize(200, 50);
            Triangle triangle = new Triangle();
            Rectangle rect = new Rectangle();

            while (!isFinished)
            {
                Console.ResetColor();
                Console.Clear();
                Console.WriteLine("1] Dodaj prostokąt");
                Console.WriteLine("2] Dodaj trójkąt");
                Console.WriteLine("3] Usuń prostokąt");
                Console.WriteLine("4] Usuń trójkąt");
                Console.WriteLine("5] Rysuj obraz");
                Console.WriteLine("6] Wyjście");

                bool correctInput = false;
                while (!correctInput)
                {
                    switch (Console.ReadKey(true).Key)
                    {
                        case ConsoleKey.D1:
                            //rect.AddRect();
                            rect.AlternativeAddRect();
                            correctInput = true;
                            break;
                        case ConsoleKey.D2:
                            //triangle.AddTriangle();
                            triangle.AlternativeAddTriangle();
                            correctInput = true;
                            break;
                        case ConsoleKey.D3:
                            rect.RemoveFromList();
                            correctInput = true;
                            break;
                        case ConsoleKey.D4:
                            triangle.RemoveFromList();
                            correctInput = true;
                            break;
                        case ConsoleKey.D5:
                            DrawPicture(true);
                            correctInput = true;
                            Console.ReadKey(true);
                            break;
                        case ConsoleKey.D6:
                            isFinished = true;
                            correctInput = true;
                            break;
                        default:
                            correctInput = false;
                            break;
                    }
                }
            }
        }
    }
}